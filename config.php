<?php

define('DB_NAME', 'singleton');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DSN', 'mysql:host=localhost;dbname=');


spl_autoload_register(function($class){
  include 'classes/' . $class . '.php';
});
