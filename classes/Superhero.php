<?php

class Superhero
{
  public function showSuperheroes($conn)
  {
    $pdos = $conn->prepare('SELECT * FROM super_heroes');
    $pdos->setFetchMode(PDO::FETCH_CLASS, 'Superhero');
    $pdos->execute();

    $res = $pdos->fetchAll();
    foreach ($res as $superhero) {
      echo '<div style="border: 1px solid red;padding:5px;margin:5px;">' . mb_convert_case($superhero->name, MB_CASE_TITLE) . ' is ' .    mb_convert_case($superhero->hero_name, MB_CASE_TITLE) . ', has power of (' . $superhero->power . ')</div>';
    }
  }
}
