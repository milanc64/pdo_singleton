<?php

require_once __DIR__ . '/../config.php';

class Singleton
{

  private static $instance;
  public static $conn;

  private function __construct()
  {
    self::$conn = new PDO(DSN . DB_NAME, DB_USER, DB_PASS);
  }

  public static function getInstance()
  {
    if(!self::$instance)
    {
      self::$instance = new Singleton();
    }

    return self::$conn;

  }
}
