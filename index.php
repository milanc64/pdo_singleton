<?php

require_once 'config.php';

$conn = Singleton::getInstance();

$superheroes = new Superhero;

$superheroes = $superheroes->showSuperheroes($conn);
